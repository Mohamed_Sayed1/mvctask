﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCTask.Models
{
    public class DepartmentVM
    {
        public int id { get; set; }
        public int Count { get; set; }
        public double SumSallary { get; set; }

    }
}