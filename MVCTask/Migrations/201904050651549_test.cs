namespace MVCTask.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Tasks", "task_ID", "dbo.Tasks");
            DropForeignKey("dbo.Tasks", "Employee_ID", "dbo.Employees");
            DropIndex("dbo.Tasks", new[] { "task_ID" });
            DropIndex("dbo.Tasks", new[] { "Employee_ID" });
            RenameColumn(table: "dbo.Tasks", name: "Employee_ID", newName: "EmployeeID");
            AlterColumn("dbo.Tasks", "EmployeeID", c => c.Int(nullable: false));
            CreateIndex("dbo.Tasks", "EmployeeID");
            AddForeignKey("dbo.Tasks", "EmployeeID", "dbo.Employees", "ID", cascadeDelete: true);
            DropColumn("dbo.Tasks", "task_ID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Tasks", "task_ID", c => c.Int());
            DropForeignKey("dbo.Tasks", "EmployeeID", "dbo.Employees");
            DropIndex("dbo.Tasks", new[] { "EmployeeID" });
            AlterColumn("dbo.Tasks", "EmployeeID", c => c.Int());
            RenameColumn(table: "dbo.Tasks", name: "EmployeeID", newName: "Employee_ID");
            CreateIndex("dbo.Tasks", "Employee_ID");
            CreateIndex("dbo.Tasks", "task_ID");
            AddForeignKey("dbo.Tasks", "Employee_ID", "dbo.Employees", "ID");
            AddForeignKey("dbo.Tasks", "task_ID", "dbo.Tasks", "ID");
        }
    }
}
