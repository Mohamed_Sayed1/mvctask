﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MVCTask.Models
{
    public class Task
    {
        public int ID { get; set; }

       
        public string TaskDescription { get; set; }
        public bool TaskStatus { get; set; }
        [ForeignKey("Employee")]
        public int EmployeeID { get; set; }
        public virtual Employee Employee { get; set; }
    }
}