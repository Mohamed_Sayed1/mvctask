﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MVCTask.Models
{
   
    public class Department
    {
      
        public int ID { get; set; }

        [Required, MaxLength(50)]
        public string Name { get; set; }
     

       

        public virtual ICollection<Employee> Employees { get; set; }
      

    }
}
