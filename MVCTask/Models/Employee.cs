﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVCTask.Models
{
    public class Employee
    {
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ManagerName { get; set; }

        public double Salary { get; set; }
     

        [NotMapped]
        public int FullName { get; set; }

        [ForeignKey("Department")]
        public int DepartmentID { get; set; }
        public virtual ICollection<Task> task { get; set; }

        public virtual Department Department { get; set; }
       
    }
}
